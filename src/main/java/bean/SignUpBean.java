package bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean
@SessionScoped
@Component
public class SignUpBean {

    private String username;
    private String password;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void doRegister() {
        System.out.println("in doRegister, username: " + username + ", password: " + password);

        if (username.length() > 0 && password.length() > 0) {
            if (!isDuplicate(username)) {
                String insertSql = "INSERT INTO users(username, password) VALUES (?,?)";
                int resultOfInsert = this.jdbcTemplate.update(insertSql, new Object[]{username, password});
                System.out.println("result Of Insert: " + resultOfInsert);
                setUsername("");
                setPassword("");
            } else {
                setPassword("");
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Duplicate Username", "Choose another username"));
            }
        }
    }

    private boolean isDuplicate(String username) {

        System.out.println("in duplicate(), username: " + username);

        String sql = " select username from users where username=?";

        List<String> results;
        results = jdbcTemplate.queryForList(sql, new Object[]{username}, String.class);

        if (results.isEmpty()) {
            System.out.println("duplicate: no");
        } else {
            System.out.println("duplicate: Yes, with: " + results.get(0));
        }

        return (!results.isEmpty());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
