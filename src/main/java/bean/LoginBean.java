package bean;


import org.springframework.security.web.WebAttributes;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Iterator;

@ManagedBean
@SessionScoped
public class LoginBean {

    private String name;
    private String password;

    public String doLogin() throws IOException, ServletException {
        //do any job with the associated values that you've got from the user, like persisting attempted login, etc.
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext extenalContext = facesContext.getExternalContext();
        RequestDispatcher dispatcher = ((ServletRequest) extenalContext.getRequest()).getRequestDispatcher("/j_spring_security_check");
        dispatcher.forward((ServletRequest) extenalContext.getRequest(), (ServletResponse) extenalContext.getResponse());
        facesContext.responseComplete();
        return null;
    }

    public void updateMessages() {
        System.out.println("Start updateMessages");
        Exception ex = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                .get(WebAttributes.AUTHENTICATION_EXCEPTION);

        if (ex != null) {
            System.out.println("ex.getMessage(): " + ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            setName("");
            setPassword("");
            Exception ex2 = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                    .remove(WebAttributes.AUTHENTICATION_EXCEPTION);

        }
        System.out.println("End updateMessages");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
