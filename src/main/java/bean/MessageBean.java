package bean;

import model.Messages;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@ManagedBean
@SessionScoped
@Component
public class MessageBean {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private String toPerson;
    private String summary;
    private String content;
    private String loggedInUser;
    private List<Messages> messagesList;
    private List<Messages> selectedMessage;


    public List<Messages> fetchMessages() {
        System.out.println("pre render...");

        String sql = "select * from messages where username=?";
        loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();

        messagesList = jdbcTemplate.query(sql, new Object[]{getLoggedInUser()},
                new BeanPropertyRowMapper<Messages>(Messages.class));

//        System.out.println("Message list : " + getMessagesList());
        return getMessagesList();
    }

//    public void fetchSelectedMessage(String msgId) {
//        System.out.println("in fetch selected msgId:" + msgId);
//        String sql = "SELECT * FROM messages WHERE msgId=?";
//        setSelectedMessage(jdbcTemplate.query(sql, new Object[]{msgId}, new BeanPropertyRowMapper<Messages>(Messages.class)));
//        System.out.println("fetched selected message== " + selectedMessage); //ok
//
//        //There is no need to open the dialog. because we do it in onComplete(...) of commandLink.
//    }

    public void addNote() {
        setLoggedInUser(SecurityContextHolder.getContext().getAuthentication().getName());
        System.out.println("toPerson: " + toPerson + " , summary: " + summary + " , content: " + content);
        System.out.println("username: " + getLoggedInUser()); // ok

        this.jdbcTemplate.update("INSERT INTO messages(username,CreatedDate,toPerson,summary,content) VALUES(?,?,?,?,?)",
                new Object[]{getLoggedInUser(), getCurrentDateTime(), toPerson, summary, content});


        this.toPerson = "";
        this.summary = "";
        this.content = "";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Note saved successfully"));
    }

    public void deleteMessage(String id) {
        System.out.println("in delete, received param id: " + id); // ok
        String sql = "delete from messages where msgId=?";
        int rows = jdbcTemplate.update(sql, new Object[]{id});
        System.out.println("deleted rows: " + rows);
    }

    public void doEditOnDB(Messages row) {
        System.out.println("in doEditOnDB , updated row: " + row);
        String sql = "update messages set toPerson=? , summary=? , content=? where msgId=?";
        int effected = jdbcTemplate.update(sql, new Object[]{row.getToPerson(), row.getSummary(),
                row.getContent(), row.getMsgId()});
        System.out.println("effected update row: " + effected);
    }

    public void onRowEdit(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Note Edited", String.valueOf(((Messages) event.getObject()).getMsgId()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
        System.out.println("updated Row: " + (event.getObject())); //ok
//        Messages edited  = new Messages();
        doEditOnDB((Messages) event.getObject());
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit canceled", String.valueOf(((Messages) event.getObject()).getMsgId()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String getCurrentDateTime() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    public String getToPerson() {
        return toPerson;
    }

    public void setToPerson(String toPerson) {
        this.toPerson = toPerson;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Messages> getMessagesList() {
        return messagesList;
    }

    public void setMessagesList(List<Messages> messagesList) {
        this.messagesList = messagesList;
    }

    public String getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(String loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public List<Messages> getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(List<Messages> selectedMessage) {
        this.selectedMessage = selectedMessage;
    }
}
