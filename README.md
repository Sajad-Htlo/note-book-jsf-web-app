Online Note Book web application.

View layer:

`JSF` - `Primefaces` (pagination - row edit)

Business layer:

`Spring security` (remember-me and DB authentication)

`Spring core`

Data access layer:

`Spring JDBC`

DBMS: `MySQL`